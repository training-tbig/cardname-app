package com.nbs.cardnameapp.domain

import com.nbs.cardnameapp.data.lib.UseCase
import com.nbs.cardnameapp.data.response.CardnameResponse
import com.nbs.cardnameapp.data.response.model.CardnameItem
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GetCardnameUseCase: UseCase() {
    lateinit var onGetCardnameCallback: OnGetCardnameCallback

    lateinit var request: Call<CardnameResponse>

    override fun callApi() {
        request = apiRequest.getCardname()
        request.enqueue(object : Callback<CardnameResponse>{
            override fun onResponse(call: Call<CardnameResponse>, response: Response<CardnameResponse>) {
                if (response.isSuccessful){
                    if (response.body()?.code == 200){
                        onGetCardnameCallback
                            .onGetCardnameSuccess(
                                response.body()?.data)
                    }else{
                        onGetCardnameCallback
                            .onGetCardnameFailed("Error in request")
                    }
                }else{
                    onGetCardnameCallback
                        .onGetCardnameFailed("Error in request")
                }
            }

            override fun onFailure(call: Call<CardnameResponse>, t: Throwable) {
                onGetCardnameCallback
                    .onGetCardnameFailed("Error in request")
            }
        })
    }

    interface OnGetCardnameCallback{
        fun onGetCardnameSuccess(list:
            MutableList<CardnameItem>?)

        fun onGetCardnameFailed(message: String)
    }
}