package com.nbs.cardnameapp.domain

import com.nbs.cardnameapp.data.lib.UseCase
import com.nbs.cardnameapp.data.response.BaseResponse
import com.nbs.cardnameapp.data.response.model.CardnameItem
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UpdateCardnameUseCase
    (private val updatedCardname: CardnameItem):
        UseCase(){

    lateinit var onUpdateCardnameCallback: OnUpdateCardnameCallback

    lateinit var request: Call<BaseResponse>

    override fun callApi() {
        request = apiRequest.updateCard(updatedCardname.id,
            updatedCardname.fullName, updatedCardname.email,
            updatedCardname.company)
        request.enqueue(object : Callback<BaseResponse>{
            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                if (response.isSuccessful){
                    if (response.body()?.code == 200) {
                        onUpdateCardnameCallback
                            .onUpdateCardnameSuccess()
                    }else{
                        onUpdateCardnameCallback.
                                onUpdateCardnameFailed("Update error")
                    }
                }else{
                    onUpdateCardnameCallback.
                        onUpdateCardnameFailed("Update error")
                }
            }

            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                onUpdateCardnameCallback.
                    onUpdateCardnameFailed("Update error")
            }
        })
    }

    interface OnUpdateCardnameCallback{
        fun onUpdateCardnameSuccess()

        fun onUpdateCardnameFailed(message: String)
    }
}