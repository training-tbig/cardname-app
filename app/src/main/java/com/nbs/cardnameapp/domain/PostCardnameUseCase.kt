package com.nbs.cardnameapp.domain

import com.nbs.cardnameapp.data.lib.UseCase
import com.nbs.cardnameapp.data.request.Cardname
import com.nbs.cardnameapp.data.response.BaseResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PostCardnameUseCase(val cardname: Cardname):
    UseCase() {

    lateinit var onPostCardnameCallback: OnPostCardnameCallback

    private lateinit var request: Call<BaseResponse>

    override fun callApi() {
        request = apiRequest.postNewCard(cardname.fullName,
            cardname.email, cardname.company)
        request.enqueue(object : Callback<BaseResponse>{

            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                if (response.isSuccessful){
                    if (response.body()?.code == 200){
                        onPostCardnameCallback
                            .onPostCardnameSuccess()
                    }else{
                        onPostCardnameCallback
                            .onPostCardnameFailed("Error post")
                    }
                }else{
                    onPostCardnameCallback
                        .onPostCardnameFailed("Error post")
                }
            }

            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                onPostCardnameCallback
                    .onPostCardnameFailed("Error post")
            }
        })
    }

    interface OnPostCardnameCallback{
        fun onPostCardnameSuccess()

        fun onPostCardnameFailed(message: String)
    }
}