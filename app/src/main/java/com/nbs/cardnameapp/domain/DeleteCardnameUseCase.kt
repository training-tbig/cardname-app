package com.nbs.cardnameapp.domain

import com.nbs.cardnameapp.data.lib.UseCase
import com.nbs.cardnameapp.data.response.BaseResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DeleteCardnameUseCase(val id: String): UseCase(){
    lateinit var onDeleteCardNameCallback: OnDeleteCardNameCallback

    lateinit var request: Call<BaseResponse>

    override fun callApi() {
        request = apiRequest.deleteCard(id)
        request.enqueue(object : Callback<BaseResponse>{
            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                if (response.isSuccessful){
                    if (response.body()?.code == 200){
                        onDeleteCardNameCallback
                            .onDeleteCardNameSuccess()
                    }else{
                        onDeleteCardNameCallback
                            .onDeleteCardNameFailed("Deletion failed")
                    }
                }else{
                    onDeleteCardNameCallback
                        .onDeleteCardNameFailed("Deletion failed")
                }
            }

            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                onDeleteCardNameCallback
                    .onDeleteCardNameFailed("Deletion failed")
            }
        })
    }

    interface OnDeleteCardNameCallback{
        fun onDeleteCardNameSuccess()

        fun onDeleteCardNameFailed(message: String)
    }
}