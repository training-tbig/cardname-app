package com.nbs.cardnameapp.data.response.model

import android.os.Parcel
import android.os.Parcelable
import android.os.Parcelable.Creator
import com.google.gson.annotations.SerializedName

data class CardnameItem(
    @SerializedName("id")
    val id: String,
    @SerializedName("full_name")
    val fullName: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("avatar_url")
    val avatarUrl: String,
    @SerializedName("company")
    val company: String
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(fullName)
        parcel.writeString(email)
        parcel.writeString(avatarUrl)
        parcel.writeString(company)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Creator<CardnameItem> {
        override fun createFromParcel(parcel: Parcel): CardnameItem {
            return CardnameItem(parcel)
        }

        override fun newArray(size: Int): Array<CardnameItem?> {
            return arrayOfNulls(size)
        }
    }
}