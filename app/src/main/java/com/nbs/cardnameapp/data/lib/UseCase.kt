package com.nbs.cardnameapp.data.lib

import com.nbs.cardnameapp.BuildConfig

abstract class UseCase {

    abstract fun callApi()

    companion object{
        val apiRequest: Repository by lazy {
            ApiService.createService(
                Repository::class.java,
                OkHttpClientFactory.create(),
                BuildConfig.BASE_URL
            )
        }
    }
}