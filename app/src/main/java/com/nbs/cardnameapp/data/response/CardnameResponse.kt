package com.nbs.cardnameapp.data.response

import com.google.gson.annotations.SerializedName
import com.nbs.cardnameapp.data.response.model.CardnameItem

class CardnameResponse(
    code: Int,
    status: String,
    @SerializedName("data")
    val data: MutableList<CardnameItem>
): BaseResponse(code, status)