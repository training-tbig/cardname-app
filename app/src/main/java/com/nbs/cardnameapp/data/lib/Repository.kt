package com.nbs.cardnameapp.data.lib

import com.nbs.cardnameapp.data.response.BaseResponse
import com.nbs.cardnameapp.data.response.CardnameResponse
import com.nbs.cardnameapp.data.response.model.CardnameItem
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Part
import retrofit2.http.Path

interface Repository {
    @FormUrlEncoded
    @POST("card")
    fun postNewCard(@Field("full_name") fullName: String,
        @Field("email") email: String,
        @Field("company") company: String): Call<BaseResponse>

    @GET("card")
    fun getCardname(): Call<CardnameResponse>

    @FormUrlEncoded
    @PUT("card/{id}")
    fun updateCard(@Path("id") id: String,
        @Field("full_name") fullName: String,
        @Field("email") email: String,
        @Field("company") company: String): Call<BaseResponse>

    @DELETE("card/{id}")
    fun deleteCard(@Path("id") id: String): Call<BaseResponse>
}