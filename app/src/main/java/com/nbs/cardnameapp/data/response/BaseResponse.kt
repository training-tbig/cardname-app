package com.nbs.cardnameapp.data.response

import com.google.gson.annotations.SerializedName

open class BaseResponse(
    @SerializedName("code")
    var code: Int,

    @SerializedName("status")
    var status: String
)