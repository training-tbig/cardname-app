package com.nbs.cardnameapp.data.request

data class Cardname(
    val fullName: String,
    val email: String,
    val company: String
)