package com.nbs.cardnameapp.presentation.main

import com.nbs.cardnameapp.data.response.model.CardnameItem
import com.nbs.cardnameapp.domain.GetCardnameUseCase

class MainPresenter(val view: MainContract.View):
    MainContract.Presenter, GetCardnameUseCase.OnGetCardnameCallback {

    lateinit var getCardnameUseCase: GetCardnameUseCase

    override fun getListCardname() {
        getCardnameUseCase = GetCardnameUseCase()
        getCardnameUseCase.onGetCardnameCallback = this

        getCardnameUseCase.callApi()
    }

    override fun onGetCardnameSuccess(list: MutableList<CardnameItem>?) {
        view.showListCardname(list)
    }

    override fun onGetCardnameFailed(message: String) {
        view.showErrorMessage(message)
    }
}