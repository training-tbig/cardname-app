package com.nbs.cardnameapp.presentation.addorupdate

import android.app.ProgressDialog
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.nbs.cardnameapp.R
import com.nbs.cardnameapp.data.response.model.CardnameItem
import com.nbs.cardnameapp.util.BUNDLE_CARDNAME
import com.nbs.cardnameapp.util.toast
import kotlinx.android.synthetic.main.activity_add_or_update.btnSubmit
import kotlinx.android.synthetic.main.activity_add_or_update.edtCompany
import kotlinx.android.synthetic.main.activity_add_or_update.edtEmail
import kotlinx.android.synthetic.main.activity_add_or_update.edtFullname

class AddOrUpdateActivity : AppCompatActivity(),
    AddOrUpdateContract.View{

    private lateinit var presenter: AddOrUpdatePresenter

    private var cardnameItem: CardnameItem ?= null

    private var isUpdate = false

    private var deleteProgressDialog:
            ProgressDialog ?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_or_update)

        cardnameItem = intent
            .getParcelableExtra(BUNDLE_CARDNAME)

        isUpdate = cardnameItem != null

        supportActionBar?.apply {
            title = if(isUpdate) "Update" else "Add"
            setDisplayHomeAsUpEnabled(true)
        }

        if (isUpdate){
            edtFullname.setText(cardnameItem?.fullName)
            edtEmail.setText(cardnameItem?.email)
            edtCompany.setText(cardnameItem?.company)
        }

        presenter = AddOrUpdatePresenter(this)

        btnSubmit.setOnClickListener {
            if (isUpdate){
                submitUpdatedCardName()
            }else{
                submitNewCardname()
            }
        }
    }

    private fun submitUpdatedCardName() {
        val fullname = edtFullname.text.toString().trim()
        val email = edtEmail.text.toString().trim()
        val company = edtCompany.text.toString().trim()

        if (fullname.isEmpty() || email.isEmpty()
            || company.isEmpty()){
            toast(this, "all fields required")
        }else{
            enableDisableViews(false)

            cardnameItem?.let {
                presenter.updateCardname(id = it.id,
                    fullname = fullname,
                    email = email,
                    company = company)
            }
        }
    }

    private fun submitNewCardname() {
        val fullname = edtFullname.text.toString().trim()
        val email = edtEmail.text.toString().trim()
        val company = edtCompany.text.toString().trim()

        if (fullname.isEmpty() || email.isEmpty()
            || company.isEmpty()){
            toast(this, "all fields required")
        }else{
            enableDisableViews(false)

            presenter.postCardname(fullname,
                email, company)
        }
    }

    override fun showPostSuccessMessage() {
        enableDisableViews(true)

        toast(this, "a new data successfully added")

        finish()
    }

    override fun showError(message: String) {
        enableDisableViews(true)

        toast(this, message)
    }

    private fun enableDisableViews(isEnabled: Boolean) {
        edtFullname.isEnabled = isEnabled
        edtCompany.isEnabled = isEnabled
        edtEmail.isEnabled = isEnabled
        btnSubmit.text = if (isEnabled) {
            "Submit"
            }
            else {
                if (isUpdate){
                    "Updating..."
                }else{
                    "Submitting..."
                }
            }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if (isUpdate){
            menuInflater.inflate(R.menu.menu_delete, menu)
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) finish()

        if (item?.itemId == R.id.action_delete)
            deleteCardname()

        return super.onOptionsItemSelected(item)
    }

    private fun deleteCardname() {
        cardnameItem?.let {
            deleteProgressDialog = ProgressDialog
                .show(this,
                "Delete item",
                "Please wait....")
            presenter.deleteCardname(it.id)
        }
    }

    override fun showUpdateSuccessMessage() {
        toast(this, "an item updated")
        finish()
    }

    override fun showDeleteSuccessMessage() {
        deleteProgressDialog?.dismiss()
        toast(this, "an item deleted")
        finish()
    }
}
