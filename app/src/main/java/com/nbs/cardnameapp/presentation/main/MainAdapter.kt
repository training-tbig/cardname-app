package com.nbs.cardnameapp.presentation.main

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nbs.cardnameapp.R
import com.nbs.cardnameapp.data.response.model.CardnameItem
import com.nbs.cardnameapp.presentation.main.MainAdapter.Viewholder
import kotlinx.android.synthetic.main.item_cardname.view.lnItem
import kotlinx.android.synthetic.main.item_cardname.view.tvCompany
import kotlinx.android.synthetic.main.item_cardname.view.tvEmail
import kotlinx.android.synthetic.main.item_cardname.view.tvFullname

class MainAdapter(val list: MutableList<CardnameItem>):
    RecyclerView.Adapter<Viewholder>() {

    lateinit var onItemClickListener: OnItemClickListener

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): Viewholder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_cardname, viewGroup,
                false)

        return Viewholder(view)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(viewholder: Viewholder,
        position: Int) {
        viewholder.bind(list[position])
    }

    inner class Viewholder(itemView: View):
        RecyclerView.ViewHolder(itemView) {

        fun bind(cardnameItem: CardnameItem){
            with(cardnameItem){
                itemView.tvFullname.text = fullName
                itemView.tvEmail.text = email
                itemView.tvCompany.text = company

                itemView.lnItem.setOnClickListener {
                    onItemClickListener.onItemClicked(cardnameItem)
                }
            }
        }
    }

    interface OnItemClickListener{
        fun onItemClicked(cardnameItem: CardnameItem)
    }
}