package com.nbs.cardnameapp.presentation.addorupdate

interface AddOrUpdateContract {
    interface View{
        fun showPostSuccessMessage()

        fun showUpdateSuccessMessage()

        fun showDeleteSuccessMessage()

        fun showError(message: String)
    }

    interface Presenter{
        fun postCardname(fullname: String,
            email: String, company: String)

        fun updateCardname(id: String, fullname: String,
            email: String, company: String)

        fun deleteCardname(id: String)
    }

}