package com.nbs.cardnameapp.presentation.addorupdate

import com.nbs.cardnameapp.data.request.Cardname
import com.nbs.cardnameapp.data.response.model.CardnameItem
import com.nbs.cardnameapp.domain.DeleteCardnameUseCase
import com.nbs.cardnameapp.domain.PostCardnameUseCase
import com.nbs.cardnameapp.domain.UpdateCardnameUseCase

class AddOrUpdatePresenter(private val view:
    AddOrUpdateContract.View):
    AddOrUpdateContract.Presenter,
    PostCardnameUseCase.OnPostCardnameCallback,
    UpdateCardnameUseCase.OnUpdateCardnameCallback,
    DeleteCardnameUseCase.OnDeleteCardNameCallback{

    private lateinit var postCardnameUseCase:
            PostCardnameUseCase

    private lateinit var updateCardnameUseCase
            : UpdateCardnameUseCase

    private lateinit var deleteCardnameUseCase:
            DeleteCardnameUseCase

    //region PostCardName
    override fun postCardname(fullname: String, email: String, company: String) {
        val cardname = Cardname(fullName = fullname,
            email = email, company = company)

        postCardnameUseCase = PostCardnameUseCase(cardname)
        postCardnameUseCase.onPostCardnameCallback = this
        postCardnameUseCase.callApi()
    }

    override fun onPostCardnameSuccess() {
        view.showPostSuccessMessage()
    }

    override fun onPostCardnameFailed(message: String) {
        view.showError(message)
    }
    //endregion

    //region UpdateCardName
    override fun updateCardname(id: String, fullname: String, email: String, company: String) {
        val updatedCardname = CardnameItem(id = id,
            fullName = fullname,
            email = email, avatarUrl = "",
            company = company)
        updateCardnameUseCase = UpdateCardnameUseCase(updatedCardname)
        updateCardnameUseCase.onUpdateCardnameCallback = this
        updateCardnameUseCase.callApi()
    }

    override fun onUpdateCardnameSuccess() {
        view.showUpdateSuccessMessage()
    }

    override fun onUpdateCardnameFailed(message: String) {
        view.showError(message)
    }
    //endregion

    //region DeleteCardName
    override fun deleteCardname(id: String) {
        deleteCardnameUseCase = DeleteCardnameUseCase(id)
        deleteCardnameUseCase.onDeleteCardNameCallback = this
        deleteCardnameUseCase.callApi()
    }

    override fun onDeleteCardNameSuccess() {
        view.showDeleteSuccessMessage()
    }

    override fun onDeleteCardNameFailed(message: String) {
        view.showError(message)
    }
    //endregion
}