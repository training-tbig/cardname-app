package com.nbs.cardnameapp.presentation.main

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.nbs.cardnameapp.R
import com.nbs.cardnameapp.R.layout
import com.nbs.cardnameapp.data.response.model.CardnameItem
import com.nbs.cardnameapp.presentation.addorupdate.AddOrUpdateActivity
import com.nbs.cardnameapp.util.BUNDLE_CARDNAME
import com.nbs.cardnameapp.util.toast
import kotlinx.android.synthetic.main.activity_main.progressBar
import kotlinx.android.synthetic.main.activity_main.rvCardname

class MainActivity : AppCompatActivity(),
    MainContract.View,
    MainAdapter.OnItemClickListener{

    private lateinit var presenter: MainPresenter

    private lateinit var adapter: MainAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout.activity_main)

        rvCardname.setHasFixedSize(true)
        rvCardname.layoutManager =
            LinearLayoutManager(this)
        rvCardname
            .addItemDecoration(DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL))

        adapter = MainAdapter(mutableListOf())
        adapter.onItemClickListener = this
        rvCardname.adapter = adapter

        presenter = MainPresenter(this)
    }

    override fun onResume() {
        super.onResume()
        presenter.getListCardname()
    }

    override fun showListCardname(list: MutableList<CardnameItem>?) {
        progressBar.visibility = View.GONE

        if (adapter.list.isNotEmpty()){
            adapter.list.clear()
            adapter.notifyDataSetChanged()
        }

        list?.let {
            adapter.list.addAll(it)
            adapter.notifyDataSetChanged()
        }
    }

    override fun showErrorMessage(message: String) {
        progressBar.visibility = View.GONE
        toast(this, message)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.action_add){
            val intent = Intent(this,
                AddOrUpdateActivity::class.java)
            startActivity(intent)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onItemClicked(cardnameItem: CardnameItem) {
        val intent = Intent(this,
            AddOrUpdateActivity::class.java)
        intent.putExtra(BUNDLE_CARDNAME, cardnameItem)
        startActivity(intent)
    }
}
