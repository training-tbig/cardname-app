package com.nbs.cardnameapp.presentation.main

import com.nbs.cardnameapp.data.response.model.CardnameItem

interface MainContract{
    interface View{
        fun showListCardname(list: MutableList<CardnameItem>?)

        fun showErrorMessage(message: String)
    }

    interface Presenter{
        fun getListCardname()
    }
}